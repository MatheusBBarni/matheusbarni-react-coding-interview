import { Button, Form, Input, message, PageHeader, Select } from 'antd';
import { useRouter } from 'next/router';
import { ValidateErrorEntity } from 'rc-field-form/lib/interface';
import { useEffect, useState } from 'react';

import { withContextInitialized } from '../../../components/hoc';
import { usePersonInformation } from '../../../components/hooks/usePersonInformation';
import CompanyCard from '../../../components/molecules/CompanyCard';
import OverlaySpinner from '../../../components/molecules/OverlaySpinner';
import GenericList from '../../../components/organisms/GenericList';

import { ResponsiveListCard } from '../../../constants';
import { Company, Person } from '../../../constants/types';

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const [person, setPerson] = useState<Person>(data);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const handleFinsh = async (values: Person) => {
    await save(values)
    router.push(`/person/${router.query?.email}`)
  }

  const handleFinshErros = (errors: ValidateErrorEntity<Person>) => {
    message.error("There are some errors in the form, please check it again!", 2);
  }

  const handleChange = (key: keyof Person, value: string) => {
    setPerson((previousState) => ({
      ...previousState,
      [key]: value
    }))
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button type="default" onClick={() => { }}>
            Cancel
          </Button>,
        ]}
      >
        {person ? (
          <Form
            name="basic"
            initialValues={{ remember: true }}
            onFinish={handleFinsh}
            onFinishFailed={handleFinshErros}
            autoComplete="off"
          >
            <Form.Item
              initialValue={person.name}
              label="Name"
              name="name"
              rules={[{ required: true, message: 'Please input your name!' }]}
            >
              <Input value={person.name} onChange={(event) => handleChange('name', event.target.value)} />
            </Form.Item>

            <Form.Item
              initialValue={person.gender}
              label="Gender"
              name="gender"
              rules={[{ required: true, message: 'Please input your gender!' }]}
            >
              <Select
                defaultValue={person.gender}
                onChange={(value) => handleChange('gender', value)}
                options={[
                  {
                    value: 'female',
                    label: 'Female',
                  },
                  {
                    value: 'male',
                    label: 'Male',
                  },
                  {
                    value: 'other',
                    label: 'other',
                  },
                ]}
              />
            </Form.Item>

            <Form.Item
              initialValue={person.phone}
              label="Phone"
              name="phone"
              rules={[{ required: true, message: 'Please input your phone!' }]}
            >
              <Input value={person.phone} onChange={(event) => handleChange('phone', event.target.value)} />
            </Form.Item>

            <Form.Item
              initialValue={person.birthday}
              label="Birthday"
              name="birthday"
              rules={[{ required: true, message: 'Please input your birthday!' }]}
            >
              <Input value={person.birthday} onChange={(event) => handleChange('birthday', event.target.value)} />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        ) : null}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => { }}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
